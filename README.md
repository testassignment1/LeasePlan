# Java Serenity Cucumber API project

This project is a Java-based testing project using Serenity and Cucumber for testing REST API endpoint.

## Prerequisites

-Java JDK 14.0.2

-Apache Maven

-Integrated Development Environment (IDE)


## Installation and Test Execution

1. Clone this repository to your local machine:

    git clone https://gitlab.com/testassignment1/LeasePlan.git

2. Open the project in your IDE.

3. Build the project using Maven: $ mvn clean install

To run the tests, execute the following command: '$ mvn clean verify' or you either just run the TestRunner test runner class.

## Test report

You can find the Serenity reports in the following directory of the Project: \target\site\serenity\ . In the serenity directory, open 'index.html' file to view the report.

## Write New Tests

To write new tests for your API project, follow these steps:

1. Create a new feature file in the `src/test/resources/features.search` directory.

2. Write your test scenarios in the feature file using Gherkin keywords (Given, When, Then).

3. Create step definitions for your test scenarios in the `src/test/java/stepdefinitions` package.

4. Implement the test logic in the step definitions using RestAssured or other libraries for making API requests.

## What was refactored

1. Feature File: I renamed 'post_product.feature' to 'get_product.feature' to align the feature file with the updated test scenarios.

2. Test Scenario: I removed the existing test scenario and wrote new ones, including positive and negative scenarios, to enhance test coverage.

3. Step Definitions: I deleted CarsAPI java class. Also I refactored step definitions and the overall code structure.

4. Search New Package: I created a new 'search' package, with a new class: 'SearchApi' (responsible for making API requests) to better organize related functionality.

5. Helpers New Package: I created a new 'helpers' package, with a new class: 'SearchHelpers, that contain common methods, assertions.

6. Model New Package: I replaced the 'starter' package with a 'model' package, which contains a new 'Product' class for modeling product data.

7. Serenity Configuration: In 'serenity.conf', I removed unnecessary WebDriver-related configurations and added a new line to point to the project's root directory.

8. File and Folder Cleanup: I deleted unused files and folders, including '.github', '.m2', '.gradle', 'history', 'build.gradle', 'gradlew', 'gradlew.bat', and 'license'.

9. Gitignore: In '.gitignore' I removed the '.gradle' entry, because we don't use gradle (we use maven).

10. POM.xml: In the project's POM.xml file, I made several updates:
    - Removed unused tags `<tags>` and `<webdriver.base.url>`
    - Deleted unused dependencies: serenity-screenplay, serenity-screenplay-webdriver, and serenity-ensure.
    - Removed `<distributionManagement>` settings.
    - Updated the 'assertj-core' dependency to version 3.24.2 to ensure compatibility.

11. README.md file: I wrote instructions in Readme file on how to install, run and write new tests and what was refactored and why
12. CI/CD pipeline: I created a new .gitlab-ci.yml file
