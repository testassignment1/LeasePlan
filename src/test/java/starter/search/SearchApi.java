package starter.search;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class SearchApi {

    Response response;
    private static final String url = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";
    public Response askForListOfProducts(String products) {

        response = SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .when()
                .get(url.concat(products));

        return response;
    }
}
