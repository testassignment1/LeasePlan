Feature: Search for the product

  As a user,
  I want to be able to search for products
  and see the correct details

  Scenario Outline: Search for existing product
    When I search for list of "<products>"
    Then I see response has 200 status code
    And I see the list of "<products>" with correct parameters
    And I see that every product has price
    Examples:
      | products |
      | apple    |
      | pasta    |
      | cola     |

  Scenario: Search for empty product
    When I search for list of "orange"
    Then I see response has 200 status code
    And I see empty list


  Scenario Outline: Search for non-existing product
    When I search for list of "<products>"
    Then I see response has 404 status code
    And Response contains message Not found
    Examples:
      | products |
      | mango    |
      | car      |



