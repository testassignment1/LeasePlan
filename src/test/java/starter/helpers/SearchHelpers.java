package starter.helpers;

import io.restassured.response.Response;
import model.Product;
import net.serenitybdd.rest.SerenityRest;
import org.assertj.core.api.SoftAssertions;
import starter.search.SearchApi;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;

public class SearchHelpers {

    SearchApi searchApi = new SearchApi();

    public void statusCodeIs(int code) {

        SerenityRest.restAssuredThat(response -> response.statusCode(code));
    }

    public void responseMessageContains(String message) {

        SerenityRest.restAssuredThat(response -> response.body("detail.message", equalTo(message)));
    }

    public void iSeeTheListOfProductsWithParameters(String products) {

        Response response = searchApi.askForListOfProducts(products);
        List<Product> productsList = Arrays.asList(response.as(Product[].class));
        SoftAssertions softAssertions = new SoftAssertions();
        for (Product product : productsList) {

            softAssertions.assertThat(product.getProvider()).isNotNull();
            softAssertions.assertThat(product.getTitle()).isNotNull();
            softAssertions.assertThat(product.getUrl()).isNotNull();
            softAssertions.assertThat(product.getBrand()).isNotNull();
            softAssertions.assertThat(product.getPrice()).isNotNull();
            softAssertions.assertThat(product.getUnit()).isNotNull();
            softAssertions.assertThat(product.getIsPromo()).isNotNull();
            softAssertions.assertThat(product.getPromoDetails()).isNotNull();
            softAssertions.assertThat(product.getImage()).isNotNull();

        }
        softAssertions.assertAll();

    }


    public void everyProductHasPrice() {

        SerenityRest.restAssuredThat(response -> response.body("price", everyItem(greaterThan(0f))));

    }

    public void emptyArray() {

        SerenityRest.restAssuredThat(response -> response.body("$", notNullValue()));
        SerenityRest.restAssuredThat(response -> response.body("$", hasSize(0)));

    }
}
