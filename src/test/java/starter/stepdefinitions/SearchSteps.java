package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.helpers.SearchHelpers;
import starter.search.SearchApi;

public class SearchSteps {

    @Steps
    SearchApi searchApi;

    @Steps
    SearchHelpers searchHelper;

    @When("I search for list of {string}")
    public void iSearchForListOf(String products) {

        searchApi.askForListOfProducts(products);

    }

    @Then("Response contains message (.*?)$")
    public void responseContainsMessageNotFound(String message) {

        searchHelper.responseMessageContains(message);
    }

    @Then("I see response has {int} status code")
    public void iSeeResponseHasStatusCode(int code) {

        searchHelper.statusCodeIs(code);
    }


    @Then("I see the list of {string} with correct parameters")
    public void iSeeTheListOfWithCorrectParameters(String product) {

        searchHelper.iSeeTheListOfProductsWithParameters(product);

    }

    @Then("I see that every product has price")
    public void iSeeThatEveryProductHasPrice() {

        searchHelper.everyProductHasPrice();

    }

    @Then("I see empty list")
    public void iSeeEmptyList() {

        searchHelper.emptyArray();

    }
}
